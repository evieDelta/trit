package trit

// BOverride Compare a list of trit values in order of last to first and returns the state of the first one it finds to be set
// Note if none are set it always returns false
func BOverride(trits ...Trit) bool {
	for i := len(trits) - 1; i >= 0; i-- {
		if trits[i].State() {
			return trits[i].Bool()
		}
	}
	return false
}

// BOverrideRev Compare a list of trit values in order of first to last and returns the state of the first one it finds to be set
// Note if none are set it always returns false
func BOverrideRev(trits ...Trit) bool {
	for _, t := range trits {
		if t.State() {
			return t.Bool()
		}
	}
	return false
}

// BAllTrue Takes a bunch of trit values and checks to see if all set ones are true, if so it returns true, if any are false it returns false
func BAllTrue(trits ...Trit) bool {
	for _, t := range trits {
		if t.State() {
			if !t.Bool() {
				return false
			}
		}
	}
	return true
}

// BAllFalse Takes a bunch of trit values and checks to see if all set ones are false, if so it returns true, if any are true it returns false
func BAllFalse(trits ...Trit) bool {
	for _, t := range trits {
		if t.State() {
			if t.Bool() {
				return false
			}
		}
	}
	return true
}

// TOverride Compare a list of trit values in order of last to first and returns the state of the first one it finds to be set
func TOverride(trits ...Trit) Trit {
	for i := len(trits) - 1; i >= 0; i-- {
		if trits[i].State() {
			return trits[i]
		}
	}
	return 0
}

// TOverrideRev Compare a list of trit values in order of first to last and returns the state of the first one it finds to be set
func TOverrideRev(trits ...Trit) Trit {
	for _, t := range trits {
		if t.State() {
			return t
		}
	}
	return 0
}
