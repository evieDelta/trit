# EvieDelta/Trit

This is a basic library for a trit (3 state bool, or a bool with a null state) system
Based around a uint8 type with a couple of helper functions to make usage easier

Proper documentation and tests to come whenever i get around to it

#### License
This code may be used under the terms any of these 3 licenses
- Unlicence (Text included in UNLICENSE file)
- CC-0
- ISC
