package trit

// Trit , Basically an int with some methods to emulate a 3 state bool, or more accurately a bool with an null/unset state
// Primary Purpose is for Setting overrides, such as overriding a global setting on a command level
type Trit uint8

// Valid states of trit.Trit
const (
	True  Trit = 1
	False Trit = 2
	Unset Trit = 0
)

// Bool returns whether or not it is true or false
// If it is unset it will always return false
func (t Trit) Bool() bool {
	if t == True {
		return true
	}
	return false
}

// State returns whether or not it is set or unset, or in simpler terms whether or not it is
func (t Trit) State() bool {
	if t == Unset {
		return false
	}
	return true
}

// Set sets the state of the trit value
// If no arguments are passed (an array len of 0) it sets it to unset
// If an argument is passed it sets it to the state off the bool
// Any arguments more than 1 are ignored
func (t *Trit) Set(states ...bool) {
	if len(states) < 1 {
		*t = Unset
		return
	}
	t.SetBool(states[0])
}

// SetBool sets the state of the trit value to either true or false based off of the state of the input argument
func (t *Trit) SetBool(state bool) {
	if state {
		*t = True
		return
	}
	*t = False
}

// Invert inverts the state of the value unless it is unset where in that case it remains the same
// The return value refers to if it has changed or not (being false if called on a "unset" value, and being true if called on a "set" (true or false) value)
func (t *Trit) Invert() bool {
	// if unset return false
	if !t.State() {
		return false
	}
	// use a switch case for the flipping
	switch *t {
	case False:
		*t = True
	case True:
		*t = False
	}
	// has changed so return true
	return true
}

// True sets the state to true
func (t *Trit) True() {
	*t = True
}

// False sets the state to false
func (t *Trit) False() {
	*t = False
}

// Unset sets the state to 'unset', or rather unsets it
func (t *Trit) Unset() {
	*t = Unset
}

// IfUnsetSet checks if the state is unset, setting it to false if it is
func (t *Trit) IfUnsetSet() {
	if !t.State() {
		t.False()
	}
}

// IfUnsetSet sets the state of t to the state of ns only if t is set to the state of 'unset'
func (t *Trit) SetIfUnset(ns bool) {
	if !t.State() {
		t.SetBool(ns)
	}
}
